##### This project is for Garena in creating a Parse-CSV-Text Tool ###########
1. Steps to run:
  - install python 2.7
  - run file main.py : "python main.py"

2. Details in project: I created 2 script python to parse CSV text
  - parse_default.py : this script uses regrex for parsing.
  - parse_tool.py :  this script uses the library: csv for parsing.
