# -*- encoding: utf-8 -*-
import re

def parse_csv(result, csv_text):
    return_value = 'Failed'
    try:
        lines = csv_text.split("\r\n")
        for line in lines:
            # fields = re.split(r""", (?=(?:"[^"]*?(?: [^"]*)*))|,(?=[^",]+(?:,|$))|, (?=[^",]+(?:,|$))""", line)
            fields = re.split(''',(?=(?:[^'"]|'[^']*'|"[^"]*")*$)''', line)
            result.append(fields)
        return_value = 'Successful'
    except Exception as e:
        print e
    return return_value
