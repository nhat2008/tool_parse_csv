# -*- encoding: utf-8 -*-
import csv

def parse_csv(result, csv_text):
    return_value = 'Failed'
    try:
        lines = csv_text.split("\r\n")
        for line in lines:
            fields = csv.reader([line], quotechar="'")
            fields = fields.next()
            result.append(fields)
        return_value = 'Successful'
    except Exception as e:
        print e
    return return_value
